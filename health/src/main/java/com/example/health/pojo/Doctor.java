package com.example.health.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="doctor")
public class Doctor {
	
@Override
	public String toString() {
		return "Doctor [id=" + id + ", name=" + name + ", exp=" + exp + ", speciliged=" + speciliged + ", type=" + type
				+ "]";
	}
@Id
@GeneratedValue
private	int id;
private	String name;
private	String exp;
private	String speciliged;
private	String type;

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getExp() {
	return exp;
}
public void setExp(String exp) {
	this.exp = exp;
}
public String getSpeciliged() {
	return speciliged;
}
public void setSpeciliged(String speciliged) {
	this.speciliged = speciliged;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
	

}
