package com.example.health.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.health.pojo.Apoinment;
@Repository
public interface AppointmentInterface  extends JpaRepository<Apoinment,Integer>{

}
