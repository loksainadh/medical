package com.example.health.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.health.pojo.Desease;
@Repository
public interface Deseaseinterface extends JpaRepository<Desease, Integer>{
	@Query("select p.name from Complaint c join c.desease d join c.visits v join v.apoinment ap join ap.patient p where d.dname=?1")
     public List<String>getdiseasenamegetpatentname(String name);
	public abstract Desease findByDeseasename(String disesename);
}
