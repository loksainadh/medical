package com.example.health.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.health.pojo.Doctor;
@Repository
public interface Doctorinterface extends JpaRepository<Doctor, Integer>{
	
}
