package com.example.health.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.health.dao.Complaininterfacedao;
import com.example.health.exptionhandler.Applicationexception;
import com.example.health.pojo.Complaint;
@Service
public class Servicecomplaininterfaceimp implements Servicecomplaininterface {
	private static Logger LOGGER=LoggerFactory.getLogger(Servicecomplaininterfaceimp.class);
@Autowired
 private Complaininterfacedao cif;

@Override
	public void savedata(Complaint cp) throws Applicationexception {
		try {
			cif.save(cp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
