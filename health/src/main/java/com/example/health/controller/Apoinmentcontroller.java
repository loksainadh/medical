package com.example.health.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.health.dao.AppointmentInterface;
import com.example.health.exptionhandler.Applicationexception;
import com.example.health.pojo.Apoinment;
import com.example.health.service.Serviceappionment;
import com.example.health.service.Serviceappionmentclassimp;
import com.example.health.service.Servicedocterinerface;




@RestController
@RequestMapping("apoinment")

public class Apoinmentcontroller {
	private static Logger LOGGER = LoggerFactory.getLogger(Apoinmentcontroller.class);
	@Autowired
	private Serviceappionment serviceappionment ;

	@PostMapping
	public ResponseEntity<String> appoinmentsave(@RequestBody Apoinment ap) throws Applicationexception {

		LOGGER.debug("entered in to  controller by appoinment save");

		ResponseEntity<String> rr = null;

		try {
			if (ap != null) {
				LOGGER.debug("appoinment==" + ap);
                
				serviceappionment.savedata(ap);
				rr = new ResponseEntity<>(HttpStatus.OK);
			}

		} catch (Exception e) {
			rr = new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		}

		LOGGER.debug("exit from  this saveap  method secessfully");

		return rr;

	}

}
