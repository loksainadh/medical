package com.example.health.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.health.exptionhandler.Applicationexception;
import com.example.health.pojo.Apoinment;
import com.example.health.pojo.Desease;
import com.example.health.service.ServiceDeseaseinterface;

@RestController
@RequestMapping("deses")
public class Desescontroller {
	private static Logger LOGGER = LoggerFactory.getLogger(Desescontroller.class);
	@Autowired
private	ServiceDeseaseinterface dinf;
	public ResponseEntity<String> desessave(@RequestBody Desease ds) throws Applicationexception{
		ResponseEntity<String> drr=null;
		try {
			dinf.save(ds);
			drr= new ResponseEntity<>(HttpStatus.OK);
		}
		catch (Exception e) {
			e.printStackTrace();
			drr= new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		LOGGER.debug("exit from  this savedeses  method secessfully");
		return drr;
		
	}
	
}
