package com.example.health.controller;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.health.exptionhandler.Applicationexception;
import com.example.health.pojo.Apoinment;
import com.example.health.pojo.Patient;
import com.example.health.service.Servicepatientinterimpclass;

import javafx.print.Collation;

@RestController
@RequestMapping("patient")

public class Patientcontroller {
	private static Logger LOGGER = LoggerFactory.getLogger(Patientcontroller.class);
	@Autowired
	private Servicepatientinterimpclass servicepatientinterimpclass;

	@PostMapping
	public ResponseEntity<String> patientsave(@RequestBody Patient pp) throws Applicationexception {

		LOGGER.debug("entered in to  controller by patent save");
		ResponseEntity<String> kk = null;
		try {
			if (kk != null) {
				LOGGER.debug("appoinment==" + pp);
				servicepatientinterimpclass.savedata(pp);
				;
				kk = new ResponseEntity<>(HttpStatus.OK);

			}
		} catch (Exception e) {
			kk = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		LOGGER.debug("exit from  this save pc  method secessfully");

		return kk;

	}

	@GetMapping("/getalldocter/{name}")
	public ResponseEntity<List<String>> getname(@PathVariable String name) throws Applicationexception {

		List<String> lp = servicepatientinterimpclass.getdata(name);
		Collections.sort(lp);
		ResponseEntity<List<String>> jp = new ResponseEntity(lp, HttpStatus.OK);
		return jp;

	}

}
